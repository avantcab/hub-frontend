# etapa de compilación
FROM node:16.13.1-alpine as build-stage
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json
RUN npm install
RUN npm install @quasar/cli -g
COPY . /app
RUN npm run build


# etapa de producción
FROM nginx:latest as production-stage
RUN rm -rf /usr/share/nginx/html/*
COPY --from=build-stage /app/dist/spa /usr/share/nginx/html
COPY nginx/nginx.conf /etc/nginx/nginx.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
