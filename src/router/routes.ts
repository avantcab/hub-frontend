import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'sync-transfers', component: () => import('pages/Index.vue') },
    ],
  },
  {
    path: '/vtc-register',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'register-transfers', component: () => import('pages/VtcRegisterTransfer.vue') },
      { path: 'point-code-zones', component: () => import('pages/ManagePointCodeZones.vue') },
    ],
  },
  {
    path: '/import-transfer',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'form', component: () => import('pages/ImportTransferForm.vue') },
      { path: 'xml', component: () => import('pages/ImportTransferXml.vue') },
      { path: 'hoppa', component: () => import('pages/ImportTransferHoppa.vue') },
      { path: 'love-holidays', component: () => import('pages/ImportTransferLoveHolidays.vue') },
    ],
  },
  {
    path: '/export-transfer',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'excel', component: () => import('pages/ExportExcelTransfers.vue') },
    ],
  },
  {
    path: '/telegram-bot',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'waybills', component: () => import('pages/TelegramWayBill.vue') },
    ],
  },
  {
    path: '/manager',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'drivers', component: () => import('pages/ManageDrivers.vue') },
      { path: 'vehicles', component: () => import('pages/ManageVehicles.vue') },
      { path: 'businesses', component: () => import('pages/ManageBusiness.vue') },
    ],
  },
  {
    path: '/login',
    component: () => import('pages/Login.vue'),
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
