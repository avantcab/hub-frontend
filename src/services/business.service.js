import { api } from 'boot/axios';
import authHeader from 'src/services/auth-header';


class BusinessService {
  fetchBusiness() {
    return api
      .get('api/business', { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }

  addBusiness(payload) {
    return api
      .post('api/business' ,
        payload,
        { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }

  updateBusiness({businessId, business}) {
    return api
      .put('api/business/' + businessId,
        { ...business },
        { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }

  deleteBusiness(businessId) {
    return api
      .delete('api/business/' + businessId,
        { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }
}

export default new BusinessService();
