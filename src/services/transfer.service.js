import { api } from 'boot/axios';
import authHeader from 'src/services/auth-header';

class TransferService {
  synchronizeEasyTaxi(payload) {
    return api
      .post('api/transfer/synchronize/easy-taxi-office',
        payload,
        { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }

  synchronizeTransunion(payload) {
    return api
      .post('api/transfer/synchronize/transunion',
        payload,
        { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }

  importTransferForm(payload) {
    return api
      .post('api/transfer/import-form',
        payload,
        { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }

  importTransferCSV(payload) {
    return api
      .post('api/transfer/import-csv',
        payload,
        { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }

  importTransfers(payload) {
    return api
      .post('api/transfer/import-hoppa-transfers',
        payload,
        { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }

  downloadTransfers(payload) {
    return api
      .get(`api/download/transfers-excel?date=${payload.date}&airportCode=${payload.airportCode}&isAssigned=${payload.isAssigned}&vehicleIds=${payload.filteredVehiclesIds}&type=${payload.providerType}`,
        { headers: authHeader(), responseType: 'blob' })
      .then(
        (response) => {
          return response.data;
        }
      );
  }
}

export default new TransferService();
