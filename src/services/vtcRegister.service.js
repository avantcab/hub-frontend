import axios from 'axios';

class VtcRegisterService {

  constructor() {
    this.apiVtc = axios.create({ baseURL: process.env.API_VTC_REST });
  }

  getPendingTransfersToRegister(date) {
    return this.apiVtc
      .post('api/transfers/pending-register', { date })
      .then(
        (response) => {
          return response.data;
        }
      );
  }

  getPendingTransfersToRegisterAuro(date) {
    return this.apiVtc
      .post('api/transfers/pending-register-auro', { date })
      .then(
        (response) => {
          return response.data;
        }
      );
  }
}

export default new VtcRegisterService();
