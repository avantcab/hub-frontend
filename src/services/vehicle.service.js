import { api } from 'boot/axios';
import authHeader from 'src/services/auth-header';


class VehicleService {
  fetchVehicles() {
    return api
      .get('api/vehicles', { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }

  addVehicle(payload) {
    return api
      .post('api/vehicles' ,
        payload,
        { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }

  updateVehicle({vehicleId, vehicle}) {
    return api
      .put('api/vehicles/' + vehicleId,
        { ...vehicle },
        { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }

  deleteVehicle(vehicleId) {
    return api
      .delete('api/vehicles/' + vehicleId,
        { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }
}

export default new VehicleService();
