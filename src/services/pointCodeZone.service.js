import axios from 'axios';

class PointCodeZoneService {

  constructor() {
    this.apiVtc = axios.create({ baseURL: process.env.API_VTC_REST });
  }

  updateAllPointCodeZone(payload) {
    return this.apiVtc
      .post('api/transfers/point-codes', payload)
      .then(
        (response) => {
          return response.data;
        }
      );
  }
}

export default new PointCodeZoneService();
