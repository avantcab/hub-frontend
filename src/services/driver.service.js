import { api } from 'boot/axios';
import authHeader from 'src/services/auth-header';


class DriverService {
  fetchDrivers() {
    return api
      .get('api/drivers', { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }

  addDriver(payload) {
    return api
      .post('api/drivers' ,
        payload,
        { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }

  updateDriver({driverId, driver}) {
    return api
      .put('api/drivers/' + driverId,
        { ...driver },
        { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }

  deleteDriver(driverId) {
    return api
      .delete('api/drivers/' + driverId,
        { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }
}

export default new DriverService();
