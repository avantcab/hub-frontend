import { api } from 'boot/axios';
import authHeader from 'src/services/auth-header';

class HoppaVerifyService {

  verifyBookingRefs(payload) {
    return api
      .post('api/hoppa/verify',
        payload,
        { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }

  rejectBookingRef(payload) {
    return api
      .post('api/hoppa/reject',
        payload,
        { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }

}

export default new HoppaVerifyService();
