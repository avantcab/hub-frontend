import { api } from 'boot/axios';
import authHeader from 'src/services/auth-header';

class ZoneReturnTimeService {
  fetchZoneReturnTimes() {
    return api
      .get('api/pickup-point-return-times', { headers: authHeader() })
      .then(
        (response) => {
          return response.data;
        }
      );
  }
}

export default new ZoneReturnTimeService();
